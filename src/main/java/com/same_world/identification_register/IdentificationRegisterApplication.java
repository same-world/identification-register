package com.same_world.identification_register;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class IdentificationRegisterApplication {

	@GetMapping("/")
	String home() {
		return "Same world - Identification Register is running";
	}

	public static void main(String[] args) {
		SpringApplication.run(IdentificationRegisterApplication.class, args);
	}
}